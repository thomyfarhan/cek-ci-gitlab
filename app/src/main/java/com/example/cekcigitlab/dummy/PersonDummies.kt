package com.example.cekcigitlab.dummy

import com.example.cekcigitlab.domain.Person

object PersonDummies {

    val listPersons: ArrayList<Person>
        get() = persons

    private val persons = arrayListOf(
        Person(0, "Gabriel Bean", "579-1525 Ridiculus Av.", 52),
        Person(1, "Rigel Berger", "302-1479 Diam Av", 32),
        Person(2, "Coby Chandler", "Ap #855-700 Quis Av.", 42),
        Person(3, "Chancellor Clark", "9381 Sit St.", 22),
        Person(4, "Geoffrey Terry", "P.O. Box 362, 8675 Sapien. Av.", 31),
        Person(5, "Timon Mcgowa", "P.O. Box 406, 8016 Quisque Rd.", 20),
        Person(6, "Robert Casey", "Ap #714-2837 Egestas St.", 25),
        Person(7, "Basil Roach", "Ap #178-9513 Non, St.", 27),
        Person(8, "Geoffrey Howard", "Ap #181-9179 Volutpat. Rd.", 28),
        Person(9, "Wang Crosby", "8770 Pede. Rd.", 66),
        Person(10, "Talon Robinson", "P.O. Box 990, 2698 Vestibulum Street", 44),
        Person(11, "Phelan Clarke", "833-1127 Risus Rd.", 38),
        Person(12, "Malik Pickett", "Ap #469-3046 Mauris Road.", 56),
        Person(13, "Elvis Lester", "987-9566 Erat. Ave", 38),
        Person(14, "Elijah Figueroa", "Ap #780-8836 Nec, Rd..", 22),
        Person(15, "Grady Ballard", "Ap #201-6989 Gravida St.", 25),
        Person(16, "Eric Bass", "8714 Neque Ave.", 27),
        Person(17, "Zachary Witt", "P.O. Box 616, 670 In Avenue", 26),
        Person(18, "Boris Mcfadden", "P.O. Box 877, 3875 Neque St.", 23),
        Person(19, "Keaton Fuentes", "757-4846 Nulla Av.", 38),
        Person(20, "Benjamin Yang", "912-7365 Mauris Rd.", 29),
        Person(21, "Timon Mcgowa", "P.O. Box 406, 8016 Quisque Rd.", 20),
        Person(22, "Robert Casey", "Ap #714-2837 Egestas St.", 25),
        Person(23, "Basil Roach", "Ap #178-9513 Non, St.", 27),
        Person(24, "Geoffrey Howard", "Ap #181-9179 Volutpat. Rd.", 28),
        Person(25, "Wang Crosby", "8770 Pede. Rd.", 66),
        Person(26, "Gabriel Bean", "579-1525 Ridiculus Av.", 52),
        Person(27, "Rigel Berger", "302-1479 Diam Av", 32),
        Person(28, "Coby Chandler", "Ap #855-700 Quis Av.", 42),
        Person(29, "Chancellor Clark", "9381 Sit St.", 22),
        Person(30, "Geoffrey Terry", "P.O. Box 362, 8675 Sapien. Av.", 31),
        Person(31, "Malik Pickett", "Ap #469-3046 Mauris Road.", 56)
    )
}