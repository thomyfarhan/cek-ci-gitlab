package com.example.cekcigitlab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.cekcigitlab.bottomNavigation.SpotifyActivity
import com.example.cekcigitlab.bottomNavigation.YoutubeActivity
import com.example.cekcigitlab.databinding.ActivityMainBinding
import com.example.cekcigitlab.pagination.PaginationActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setClickButton()
    }

    private fun setClickButton() {
        binding.btnMainPagination.setOnClickListener {
            startActivity(Intent(this, PaginationActivity::class.java))
        }

        binding.btnMainSpotify.setOnClickListener {
            startActivity(Intent(this, SpotifyActivity::class.java))
        }

        binding.btnMainYoutube.setOnClickListener {
            startActivity(Intent(this, YoutubeActivity::class.java))
        }
    }
}
