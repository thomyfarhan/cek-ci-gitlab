package com.example.cekcigitlab.domain

data class Person (
    val id: Long?,
    val name: String?,
    val address: String?,
    val age: Int?,
    var listType: PersonListType = PersonListType.VIEW_TYPE_ITEM
)

enum class PersonListType {
    VIEW_TYPE_ITEM,
    VIEW_TYPE_LOADING,
    VIEW_TYPE_LOAD_MORE
}