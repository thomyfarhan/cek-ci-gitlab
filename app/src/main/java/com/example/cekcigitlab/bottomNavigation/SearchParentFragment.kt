package com.example.cekcigitlab.bottomNavigation

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

import com.example.cekcigitlab.R

class SearchParentFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_parent, container, false)
    }

    fun isCurrentStartDestination(): Boolean {
        return Navigation.findNavController(requireActivity(), R.id.nav_host_search).graph.startDestination ==
                Navigation.findNavController(requireActivity(), R.id.nav_host_search).currentDestination?.id
    }

}
