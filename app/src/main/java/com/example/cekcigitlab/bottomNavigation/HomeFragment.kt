package com.example.cekcigitlab.bottomNavigation

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.example.cekcigitlab.R
import com.example.cekcigitlab.databinding.FragmentHomeBinding
import com.example.cekcigitlab.dummy.PersonDummies
import com.example.cekcigitlab.pagination.PaginationPersonAdapter

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        initRecyclerView()

        return binding.root
    }

    private fun initRecyclerView() {
        val personAdapter = PaginationPersonAdapter()
        binding.rvHomePerson.adapter = personAdapter

        personAdapter.submitList(PersonDummies.listPersons)
    }

    fun scrollToTop() {
        binding.rvHomePerson.smoothScrollToPosition(0)
    }


}
