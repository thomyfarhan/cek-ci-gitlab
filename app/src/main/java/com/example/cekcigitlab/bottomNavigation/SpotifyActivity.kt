package com.example.cekcigitlab.bottomNavigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.cekcigitlab.R
import com.example.cekcigitlab.databinding.ActivitySpotifyBinding

class SpotifyActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySpotifyBinding
    private var currentBottomNavItem = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_spotify)

        setStatusBar()
        initializeFragments()
        setUpSelectedDestination()
    }

    private fun setStatusBar() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorViewBlack)
    }

    private fun initializeFragments() {
        binding.bnSpotify.setOnNavigationItemSelectedListener {
            currentBottomNavItem = it.itemId

            when (it.itemId) {
                R.id.home_destination -> {
                    Navigation.findNavController(this, R.id.nav_host_spotify)
                        .currentDestination?.id?.let { id ->

                        when (id) {
                            R.id.home_destination -> {
                                val fragment = this.getFragment(HomeFragment::class.java)
                                fragment?.scrollToTop()
                                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
                            }
                            R.id.search_parent_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    SearchParentFragmentDirections.actionSearchParentToHome()
                                )
                            }
                            R.id.library_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    LibraryFragmentDirections.actionLibraryToHome()
                                )
                            }
                        }
                    }
                    true
                }
                R.id.search_parent_destination -> {
                    Navigation.findNavController(this, R.id.nav_host_spotify)
                        .currentDestination?.id?.let { id ->

                        when (id) {
                            R.id.home_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    HomeFragmentDirections.actionHomeToSearchParent()
                                )
                            }
                            R.id.search_parent_destination -> {
                                val fragment = this.getFragment(SearchParentFragment::class.java)
                                if (fragment != null) {
                                    if (fragment.isCurrentStartDestination()) {
                                        Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show()
                                    } else {
                                        Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                            SearchParentFragmentDirections.actionSearchParentSelf()
                                        )
                                    }
                                }
                            }
                            R.id.library_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    LibraryFragmentDirections.actionLibraryToSearchParent()
                                )
                            }
                        }
                    }
                    true
                }
                R.id.library_destination -> {
                    Navigation.findNavController(this, R.id.nav_host_spotify)
                        .currentDestination?.id?.let { id ->

                        when (id) {
                            R.id.home_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    HomeFragmentDirections.actionHomeToLibrary()
                                )
                            }
                            R.id.search_parent_destination -> {
                                Navigation.findNavController(this, R.id.nav_host_spotify).navigate(
                                    SearchParentFragmentDirections.actionSearchParentToLibrary()
                                )
                            }
                            R.id.library_destination -> {
                                Toast.makeText(this, "Library", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun setUpSelectedDestination() {
        Navigation.findNavController(this, R.id.nav_host_spotify)
            .addOnDestinationChangedListener { _, destination, _ ->

            Log.d("besok", "${destination.id}")
            when(destination.id) {
                R.id.home_destination -> {
                    if (currentBottomNavItem != R.id.home_destination) {
                        binding.bnSpotify.menu.getItem(0).isChecked = true
                        currentBottomNavItem = R.id.home_destination
                    }
                }
                R.id.search_parent_destination -> {
                    if (currentBottomNavItem != R.id.search_parent_destination) {
                        binding.bnSpotify.menu.getItem(1).isChecked = true
                        currentBottomNavItem = R.id.search_parent_destination
                    }
                }
                R.id.library_destination -> {
                    if (currentBottomNavItem != R.id.library_destination) {
                        binding.bnSpotify.menu.getItem(2).isChecked = true
                        currentBottomNavItem = R.id.library_destination
                    }
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <F : Fragment> AppCompatActivity.getFragment(fragmentClass: Class<F>): F? {
        val navHostFragment = this.supportFragmentManager.fragments.first() as NavHostFragment

        navHostFragment.childFragmentManager.fragments.forEach {
            if (fragmentClass.isAssignableFrom(it.javaClass)) {
                return it as F
            }
        }

        return null
    }

}
