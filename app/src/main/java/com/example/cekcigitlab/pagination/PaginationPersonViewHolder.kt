package com.example.cekcigitlab.pagination

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cekcigitlab.databinding.ListLoadMoreBinding
import com.example.cekcigitlab.databinding.ListPaginationPersonBinding
import com.example.cekcigitlab.databinding.ListProgressBarBinding
import com.example.cekcigitlab.domain.Person

open class PaginationPersonViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    class PaginationPersonItemViewHolder(private val binding: ListPaginationPersonBinding):
            PaginationPersonViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PaginationPersonItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListPaginationPersonBinding
                    .inflate(layoutInflater, parent, false)
                return PaginationPersonItemViewHolder(binding)
            }
        }

        fun bind(person: Person) {
            Log.d("besok", "tell me")
            binding.tvPaginationPersonName.text = person.name
            binding.tvPaginationPersonAddress.text = person.address
            binding.tvPaginationPersonAge.text = person.age.toString()
        }
    }

    class PaginationPersonLoadingViewHolder(private val binding: ListProgressBarBinding):
            PaginationPersonViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PaginationPersonLoadingViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListProgressBarBinding
                    .inflate(layoutInflater, parent, false)
                return PaginationPersonLoadingViewHolder(binding)
            }
        }

        fun bind() {
            binding.pbListProgress.isIndeterminate = true
        }
    }

    class PaginationPersonLoadMoreViewHolder(binding: ListLoadMoreBinding):
            PaginationPersonViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PaginationPersonLoadMoreViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListLoadMoreBinding
                    .inflate(layoutInflater, parent, false)
                return PaginationPersonLoadMoreViewHolder(binding)
            }
        }

        fun bind() {}
    }
}