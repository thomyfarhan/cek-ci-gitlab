package com.example.cekcigitlab.pagination

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.cekcigitlab.domain.Person
import com.example.cekcigitlab.domain.PersonListType

class PaginationPersonAdapter: ListAdapter<Person,
        PaginationPersonViewHolder>(PaginationPersonDiffCallback()) {

    companion object {
        const val TYPE_ITEM = 0
        const val TYPE_LOADING = 1
        const val TYPE_LOAD_MORE = 2
    }

    override fun getItemViewType(position: Int): Int {
        return when(getItem(position).listType) {
            PersonListType.VIEW_TYPE_ITEM -> TYPE_ITEM
            PersonListType.VIEW_TYPE_LOADING -> TYPE_LOADING
            PersonListType.VIEW_TYPE_LOAD_MORE -> TYPE_LOAD_MORE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaginationPersonViewHolder {
        return when(viewType) {
            TYPE_ITEM -> PaginationPersonViewHolder.PaginationPersonItemViewHolder.from(parent)
            TYPE_LOADING -> PaginationPersonViewHolder.PaginationPersonLoadingViewHolder.from(parent)
            TYPE_LOAD_MORE -> PaginationPersonViewHolder.PaginationPersonLoadMoreViewHolder.from(parent)
            else -> throw ClassCastException("Unknown ViewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: PaginationPersonViewHolder, position: Int) {
        when(holder) {
            is PaginationPersonViewHolder.PaginationPersonItemViewHolder -> holder.bind(getItem(position))
            is PaginationPersonViewHolder.PaginationPersonLoadingViewHolder -> holder.bind()
            is PaginationPersonViewHolder.PaginationPersonLoadMoreViewHolder -> holder.bind()
        }
    }
}

class PaginationPersonDiffCallback: DiffUtil.ItemCallback<Person>() {
    override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem == newItem
    }

}