package com.example.cekcigitlab.pagination

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cekcigitlab.R
import com.example.cekcigitlab.databinding.ActivityPaginationBinding

class PaginationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPaginationBinding
    private lateinit var viewModel: PaginationViewModel
    private lateinit var personAdapter: PaginationPersonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pagination)

        initViewModel()
        initRecyclerView()
        populatePersonList()
        setRecyclerViewBehavior()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(PaginationViewModel::class.java)
    }

    private fun initRecyclerView() {
        personAdapter = PaginationPersonAdapter()
        binding.rvPaginationPerson.adapter = personAdapter
    }

    private fun populatePersonList() {
        viewModel.persons.observe(this, Observer {
            it?.let { persons ->
                personAdapter.submitList(persons)
            }
        })
    }

    private fun setRecyclerViewBehavior() {
        val personLayoutManager = binding.rvPaginationPerson.layoutManager as LinearLayoutManager

        binding.rvPaginationPerson.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItem = personLayoutManager.itemCount
                val lastVisibleItem = personLayoutManager.findLastVisibleItemPosition()
                if (lastVisibleItem >= totalItem - 4) {
                    viewModel.addMorePersonsData()
                }
            }
        })
    }


}
