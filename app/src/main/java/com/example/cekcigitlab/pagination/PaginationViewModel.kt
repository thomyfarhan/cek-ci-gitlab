package com.example.cekcigitlab.pagination

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.cekcigitlab.domain.Person
import com.example.cekcigitlab.domain.PersonListType
import com.example.cekcigitlab.dummy.PersonDummies
import java.util.*
import kotlin.collections.ArrayList

class PaginationViewModel: ViewModel() {

    private val _persons = MutableLiveData<List<Person>>()
    val persons: LiveData<List<Person>>
        get() = _persons

    private var personListState = 0
    private var isRefreshable = true

    init {
        initializePersonsData()
    }

    private fun initializePersonsData() {
        val list = ArrayList<Person>()
        addDataToPersonList(list)
    }

    fun addMorePersonsData() {
        _persons.value?.let { personsList ->
            if (isRefreshable) {
                addLoadingToPersonList(personsList)
                Timer().schedule(object: TimerTask() {
                    override fun run() {
                        removeLoadingFromPersonList()
                        addDataToPersonList(personsList)
                    }
                }, 2000)
            }
        }
    }

    private fun addDataToPersonList(list: List<Person>) {
        val targetSize = if (PersonDummies.listPersons.size - personListState > 10) {
            10
        } else {
            PersonDummies.listPersons.size - personListState
        }

        val personList = ArrayList<Person>()
        personList.addAll(list)
        for (size in personListState until personListState + targetSize) {
            personList.add(PersonDummies.listPersons[size])
        }

        _persons.postValue(personList)
        personListState += targetSize

        isRefreshable = targetSize >= 10
    }

    private fun addLoadingToPersonList(list: List<Person>) {
        val personList = ArrayList<Person>()
        val id = list.size.toLong()
        personList.addAll(list)
        personList.add(Person(id, null, null, null, PersonListType.VIEW_TYPE_LOADING))

        _persons.value = personList
        isRefreshable = false
    }

    private fun removeLoadingFromPersonList() {
        val list = ArrayList<Person>()
        persons.value?.let { personsList ->
            list.addAll(personsList.filter { it.listType == PersonListType.VIEW_TYPE_ITEM })
        }

        _persons.postValue(list)
    }


}